import hashlib
import phonenumbers
import logging
import ssl
import requests
from random import randint

from requests.packages.urllib3.poolmanager import PoolManager

from . import elp_settings

logging.basicConfig()
logger = logging.getLogger(__name__)
logger.setLevel(logging.DEBUG)


class QueryResult(object):    
    def __init__(self):
        self.ok = False
        self.mobile_number = None
        self.request_headers = None
        self.request_payload = None
        self.response_status = None
        self.response_headers = None
        self.response_payload = None
        self.parsed_data = None
        self.description = None
        self.message = None


class DictToObj(object):
    def __init__(self, settings=None):
        if settings is not None and len(settings) > 0:
            for el in settings:
                setattr(self, el, settings[el])
        
    def __repr__(self):
        return "<DictToObj: %s >" % self.__dict__


def call_remote(fn, payload, session, request_obj, timeout, *args, **kwargs):
    retval = fn(session=session, request_obj=request_obj, timeout=timeout)
    return retval


def send_request(session, request_obj, timeout):
    logger.info('Sending request...')
    resp = session.send(request_obj, timeout=int(timeout), verify=False)
    # resp = session.send(request_obj, timeout=int(timeout))
    logger.info('Done, returning...')
    return resp


def generate_reference_code():
    s = ''
    for i in range(16):
        s += str(randint(0, 9))
    return s

def create_token(txtimestamp=None, branchid=None, corporateid=None, secretkey=None):
    """
    Creates transaction key

    :param txtimestamp: transaction timestamp
    :param branchid: branch id
    :param corporateid: corporate id
    :param secretkey: secretkey
    :returns: transaction key
    :rtype: string
    """
    logger.info('Constructing client credentials')
    if not txtimestamp or not branchid or not corporateid or not secretkey:
        logger.warning('Credentials does not exist')
        return None

    non_sorted = [str(txtimestamp), str(branchid), str(corporateid)]
    logger.info('Original param orders: {}'.format(non_sorted))

    '''(Step 1) sort values'''
    value1, value2, value3 = sorted(non_sorted)
    logger.info("Step 1 Sorted params: {}, {}, {}".format(value1, value2, value3))

    '''(Step 2) md5 hash the first value'''
    hashedvalue1 = hashlib.md5(value1.encode('utf-8')).hexdigest()
    logger.info('Step 2 hashedvalue1 MD5: {}'.format(hashedvalue1))

    '''(Step 3) reverse hash2 string'''
    rev_value2 = value2[::-1]
    logger.info('Step 3 reverse value2: {} -> {}'.format(value2, rev_value2))

    '''(Step 4 & 5) concatenate previous values'''
    hashval2 = hashval = str(hashedvalue1) + str(rev_value2)
    logger.info('Step 4 concat hashedvalue1 and reversed value2: {}'.format(hashval))
    logger.info('Step 5 duplicated hash string: {}'.format(hashval2))

    '''(Step 6) concatenate duplicated hash string'''
    hashval += str(hashval2)
    logger.info('Step 6 concat dupe strings: {}'.format(hashval))

    '''(Step 7) append secret key to hash string'''
    hashval += str(secretkey)
    logger.info('Step 7 append secret key {}'.format(hashval))

    '''(Step 8) get md5 value of the current hash string and append'''
    hashedsecret = hashlib.md5(hashval.encode('utf-8')).hexdigest()
    logger.info('Step 8 hashed hashval + secret: {}'.format(hashedsecret))

    '''(Step 9) append hashedsecret with hashval'''
    hashval += hashedsecret
    logger.info('Step 9 append hashval with hashedsecret: {}'.format(hashval))

    '''(Step 10) reverse the hash3 string'''
    rev_value3 = value3[::-1]
    logger.info('Step 10 reverse hash3: {}'.format(rev_value3))

    '''(Step 11) get SHA1 value of reversed hash3 string'''
    hash_rev_value3 = hashlib.sha1(rev_value3.encode('utf-8')).hexdigest()
    logger.info('Step 11 reversed value3 SHA1 value: {}'.format(hash_rev_value3))

    '''(Step 12) append hashed hash3 to current hash string'''
    hashval += str(hash_rev_value3)
    logger.info('Step 12 hash string + rev_hash3 hash: {}'.format(hashval))

    '''(Step 13) get SHA256 value of the current hash string'''
    txkey = hashlib.sha256(hashval.encode('utf-8')).hexdigest()
    logger.info('Step 13 hashval SHA256: {}'.format(txkey))

    return txkey


def to_national_format(mobile_number):
    ro_num = phonenumbers.parse(mobile_number, 'PH')
    fmn = phonenumbers.format_number(ro_num, phonenumbers.PhoneNumberFormat.NATIONAL)
    return fmn.replace(' ', '')
