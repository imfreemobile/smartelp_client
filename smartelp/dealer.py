import requests
import logging

from lxml import etree as et

from . import elp_settings
from .loggers import LogFileHandler, ELPTXLogger
from .core import SmartAPI
from .helpers import (QueryResult, create_token, call_remote, send_request)
from .constants import respCodes
from .exceptions import ELPException


logging.basicConfig()
logger = logging.getLogger(__name__)
logger.setLevel(logging.DEBUG)

txlogger = ELPTXLogger.setup_logger()

logging.getLogger('requests').setLevel(logging.CRITICAL)


class Dealer(SmartAPI):

    def __init__(self, settings=None):
        super(Dealer, self).__init__(settings)

    def get_balance(self, **kwargs):
        """
        Get remaining balance of Dealer entity

        :param kwargs: key-value arguments of unknown purpose
        :return status: status of the inquiry
        :return resp: response tuple or None
        :return desc: response code description
        """
        return super(Dealer, self).get_balance(inquiry_type='DLRBAL', **kwargs)

    def topup_retailer_account(self, amount, reference_number=None, **kwargs):
        """
        Transfer fund from Dealer to retailer

        :param amount: amount to be transfered
        :param kwargs: optional arguments for non-defualt values. May include 'retailer_account', 'terminal_id' and 'address'
        :return status: query status
        :return resp: response tuple or None
        :return desc: response code description

        For Dealer to Retailer Wallet Transfer transactions, default value is DLR2RTL.
        """
        operation = 'DLR2RTL'
        txtimestamp = self._get_timestamp()
        if not reference_number:
            reference_number = self._get_reference_number()

        payload = {}
        payload['corp_id'] = self._config.SMARTELP_CORPORATE_ID.strip()
        payload['branch_id'] = self._config.SMARTELP_BRANCH_ID.strip()
        payload['transaction_key'] = create_token(secretkey=self._config.SMARTELP_SECRET_KEY.strip(),
                                                  corporateid=self._config.SMARTELP_CORPORATE_ID.strip(),
                                                  branchid=self._config.SMARTELP_BRANCH_ID.strip(),
                                                  txtimestamp=txtimestamp)
        payload['reference_number'] = reference_number
        payload['amount'] = amount
        payload['transaction_type'] = 'DLR2RTL'
        payload['dealer_card_number'] = self._config.SMARTELP_DEALER_ACC_NO
        payload['target_retailer_account'] = kwargs.get('retailer_account',
                                                         self._config.SMARTELP_DEFAULT_RETAILER_ACC_NO)
        payload['merchant_sender_name'] = self.merchSenderNameFlag
        payload['merchant_cross_sell'] = ''
        payload['request_timestamp'] = txtimestamp
        payload['terminal_id'] = kwargs.get('terminal_id', self._config.SMARTELP_DEFAULT_POS_TERMINAL_ID)
        payload['address'] = kwargs.get('address', self._config.SMARTELP_DEFAULT_POS_ADDRESS)

        xml_payload = self.__prepare_transfer_fund_form(**payload)
        credentials = {'username': self._config.SMARTELP_DEALER2RETAILER_USERNAME,
                       'password': self._config.SMARTELP_DEALER2RETAILER_PASSWORD}

        headers = self._header_setup(**credentials)

        req = self._create_request(self._config.SMARTELP_DEALER2RETAILER_URL,
                                   xml_payload,
                                   headers)

        logger.info("Headers: {}".format(headers))
        logger.info("Endpoint@: {}".format(self._config.SMARTELP_DEALER2RETAILER_URL))
        
        s = requests.Session()
        result = QueryResult()

        result.request_headers = headers
        result.request_payload = xml_payload

        try:
            res = call_remote(fn=send_request,
                              payload=payload,
                              session=s,
                              request_obj=req,
                              timeout=self._config.SMARTELP_REQUEST_TIMEOUT)

            logger.info("Status: {}".format(res.status_code))
            logger.info("Headers: {}".format(res.headers))
            logger.info("Response: {}".format(res.content))

            result.response_status = res.status_code
            result.response_headers = res.headers

            if res.content is not None:
                result.response_payload = res.content

            if res.status_code == 200:
                if res.content is None:
                    result.message = result.description = "Empty payload"
                    raise ELPException()
                if res.headers['X-Backside-Transport'].startswith('FAIL'):
                    result.message = "Transaction failed"
                    if res.content:
                        pr = self.__parse_dl2rt_fund_transfer_response(res.content)
                        desc = respCodes[pr['response_code']]
                        result.parsed_data = pr
                        result.description = desc
                    raise ELPException()
                pr = self.__parse_dl2rt_fund_transfer_response(res.content)
                desc = respCodes[pr['response_code']]
                if pr['response_code'] != '0000':
                    result.description = desc
                    raise ELPException()
                result.ok = True
                result.parsed_data = pr
                result.description = desc
                txlogger.info("ELP {} Transaction complete.".format(operation),
                              extra={'user': 'SYSTEM',
                                     'operation': operation,
                                     'request_headers': result.request_headers,
                                     'request_payload': result.request_payload,
                                     'status_code': result.response_status,
                                     'response_headers': result.response_headers,
                                     'response_payload': result.response_payload,
                                     'description': result.description,
                                     'status': 'SUCCESS'})
                logger.info("ELP {} Transaction complete.".format(operation))
                return result
            elif res.status_code == 401:
                if res.content is None:
                    result.message = result.description = "Empty payload"
                    raise ELPException()
                logger.info('Got error response')
                resp_dict = self._parse_error_response(res.content)
                desc = str(resp_dict['faultcode']) + str(' - ' + resp_dict['faultstring'])
                result.ok = False
                result.parsed_data = None
                result.description = desc
                raise ELPException()
            elif res.status_code == 500:
                logger.warning("Failed logon response, STATUS: {} HEADER: {} RESPONSE: {}".format(res.status_code,
                                                                                                  res.headers,
                                                                                                  res.content))
                result.ok = False
                result.parsed_data = None
                result.description = 'API Transaction Failure'
                raise ELPException()
            else:
                logger.warning("Unknown response, STATUS: {} HEADER: {} RESPONSE: {}".format(res.status_code,
                                                                                             res.headers,
                                                                                             res.content))
                result.ok = False
                result.parsed_data = None
                result.description = 'Unknown response error'
                raise ELPException()
        except ELPException as err:
            txlogger.error("ELP {} Transaction failed. {}: {}".format(operation,
                                                                      err,
                                                                      result.description),
                           extra={'user': 'SYSTEM',
                                  'operation': operation,
                                  'request_headers': result.request_headers,
                                  'request_payload': result.request_payload,
                                  'status_code': result.response_status,
                                  'response_headers': result.response_headers,
                                  'response_payload': result.response_payload,
                                  'description': result.description,
                                  'status': 'FAILED'})
            logger.error("ELP {} Transaction failed. {}: {}".format(operation,
                                                                    err,
                                                                    result.description))
            return result
        except Exception as err:
            txlogger.exception("ELP {} Transaction failed. {}: {}".format(operation,
                                                                          err,
                                                                          result.description),
                               extra={'user': 'SYSTEM',
                                      'operation': operation,
                                      'request_headers': result.request_headers,
                                      'request_payload': result.request_payload,
                                      'status_code': result.response_status,
                                      'response_headers': result.response_headers,
                                      'response_payload': result.response_payload,
                                      'description': result.description,
                                      'status': 'FAILED'})
            logger.exception("ELP {} Transaction failed. {}: {}".format(operation,
                                                                        err,
                                                                        result.description))
            return result
###

    def __prepare_transfer_fund_form(self, **kwargs):
        """
        Dealer to Retailer Fund Transfer form generation
        - This transaction allows the Dealer to transfer a particular amount to the Retailer wallet.
        :param kwargs:
        :return: xml_data
        """

        xml_data = """<?xml version="1.0" encoding="UTF-8"?>
        <soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:smart="http://smart.com.ph/">
            <soapenv:Header/>
            <soapenv:Body>
                <smart:dealerToRetailerRequest>
                    <corporateID>{corp_id}</corporateID>
                    <branchID>{branch_id}</branchID>
                    <transactionKey>{transaction_key}</transactionKey>
                    <requestRefNo>{reference_number}</requestRefNo>
                    <amount>{amount}</amount>
                    <transactionType>{transaction_type}</transactionType>
                    <dealerCardNumber>{dealer_card_number}</dealerCardNumber>
                    <targetRetAccount>{target_retailer_account}</targetRetAccount>
                    <requestTimestamp>{request_timestamp}</requestTimestamp>
                    <terminalID>{terminal_id}</terminalID>
                    <address>{address}</address>
                </smart:dealerToRetailerRequest>
            </soapenv:Body>
        </soapenv:Envelope>
        """.format(**kwargs)
        logger.info(xml_data)
        return xml_data

    def __parse_dl2rt_fund_transfer_response(self, data):
        logger.info("Parsing dealer to retailer transfer response: {}".format(str(data)))
        response_dict = {}
        xml_response = et.fromstring(data)

        response_dict['corporate_id'] = xml_response.find('.//corporateID').text
        response_dict['branch_id'] = xml_response.find('.//branchID').text
        response_dict['reference_no'] = xml_response.find('.//requestRefNo').text
        response_dict['dealer_card_no'] = xml_response.find('.//dealerCardNumber').text
        response_dict['retailer_account'] = xml_response.find('.//targetRetAccount').text
        response_dict['retailer_balance'] = xml_response.find('.//retailerNewBalance').text
        response_dict['amount'] = xml_response.find('.//amount').text
        response_dict['dealer_balance'] = xml_response.find('.//dealerNewBalance').text
        response_dict['response_code'] = xml_response.find('.//respCode').text
        response_dict['response_desc'] = xml_response.find('.//respCodeDesc').text
        response_dict['transaction_no'] = xml_response.find('.//transactionRRN').text
        response_dict['txtimestamp'] = xml_response.find('.//transactionTimestamp').text
        return response_dict
