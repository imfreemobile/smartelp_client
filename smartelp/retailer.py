import requests
import logging

from lxml import etree as et
from decimal import Decimal

from . import elp_settings
from .loggers import LogFileHandler, ELPTXLogger
from .core import SmartAPI
from .helpers import (QueryResult, create_token, call_remote, send_request, to_national_format)
from .constants import respCodes
from .exceptions import ELPException

logging.basicConfig()
logger = logging.getLogger(__name__)
logger.setLevel(logging.DEBUG)

txlogger = ELPTXLogger.setup_logger()

logging.getLogger('requests').setLevel(logging.CRITICAL)


class Retailer(SmartAPI):
    """
    """

    def __init__(self, settings=None):
        super(Retailer, self).__init__(settings)

    def get_balance(self, inquiry_type="RTLBAL", **kwargs):
        """
        Get remaining balance of Retailer entity

        :param inquiry_type: (RTLBAL or RTLBALSUN)
        :param kwargs: key-value arguments of unknown purpose
        :return status: status of the inquiry
        :return resp: response tuple or None
        :return desc: response code description
        """
        return super(Retailer, self).get_balance(inquiry_type=inquiry_type, **kwargs)

    def topup_subscriber(self, plan, mobile_number, amount=None, transaction_type="RTL2SUB", reference_number=None, **kwargs):
        """
        Transfer fund from Retailer to Subscriber

        :param plan: plan code to be availed to subscriber
        :param mobile_number: mobile number of the subscriber
        :param amount: amount to be transfered
        :param transaction_type: RTL2SUB or RTL2SUBSUN
        :param kwargs: optional arguments for non-defualt values.
        :return status: query status
        :return resp: response tuple or None
        :return desc: response code description

        For Retailer to Subscriber Wallet Transfer transactions, default value is DLR2RTLRTL.
        """
        operation = 'RTL2SUB'
        txtimestamp = self._get_timestamp()
        if not reference_number:
            reference_number = self._get_reference_number()
        payload = dict()
        flexiload = False

        if plan.lower() == 'myload':
            payload['unit_credit'] = str(Decimal(amount).quantize(Decimal('0.01')))
            flexiload = True

        payload['corp_id'] = self._config.SMARTELP_CORPORATE_ID.strip()
        payload['branch_id'] = self._config.SMARTELP_BRANCH_ID.strip()
        payload['transaction_key'] = create_token(secretkey=self._config.SMARTELP_SECRET_KEY.strip(),
                                                  corporateid=self._config.SMARTELP_CORPORATE_ID.strip(),
                                                  branchid=self._config.SMARTELP_BRANCH_ID.strip(),
                                                  txtimestamp=txtimestamp)
        payload['reference_number'] = reference_number
        payload['plan_code'] = plan  # ???Product Code or Keyword of the product offer to be loaded to the subscriber.
        """
        Type of service being requested by the partner.
            RTL2SUB = Reload Product Offer to Subscriber and Flexiload transactions
            RTLSLD = Retailer Debit transactions.
            RTLSLDREV = Retailer Debit reversal.
        """
        payload['transaction_type'] = transaction_type
        # payload['sourceAccount'] = ''  # ??? M If Transaction Type= RTLSLD or RTLSLDREV
        # payload['amountDeduct'] = ''  # ??? M (If Transaction Type= RTLSLD)
        payload['targetCheckFlag'] = 'N'  # ??? M (If Transaction Type= RTLSLD)
        # payload['transactionRRN'] = ''  # ??? M (If Transaction Type= RTLSLDREV)
        payload['target_account'] = to_national_format(mobile_number=mobile_number)
        payload['request_timestamp'] = txtimestamp
        payload['terminal_id'] = kwargs.get('terminal_id',
                                            self._config.SMARTELP_DEFAULT_POS_TERMINAL_ID)
        payload['address'] = kwargs.get('address', self._config.SMARTELP_DEFAULT_POS_ADDRESS)
        payload['zip_code'] = ''  # optional

        xml_payload = self.__prepare_reload_form(flexiload=flexiload, **payload)
        credentials = {'username': self._config.SMARTELP_RETAILER2SUBSCRIBER_USERNAME,
                       'password': self._config.SMARTELP_RETAILER2SUBSCRIBER_PASSWORD}

        headers = self._header_setup(**credentials)
        req = self._create_request(self._config.SMARTELP_RETAILER2SUBSCRIBER_URL,
                                   xml_payload,
                                   headers)

        logger.info("Headers: {}".format(headers))
        logger.info("Endpoint@: {}".format(self._config.SMARTELP_RETAILER2SUBSCRIBER_URL))

        s = requests.Session()
        result = QueryResult()

        result.request_headers = headers
        result.request_payload = xml_payload
        result.mobile_number=mobile_number

        try:
            res = call_remote(fn=send_request,
                              payload=payload,
                              session=s,
                              request_obj=req,
                              timeout=self._config.SMARTELP_REQUEST_TIMEOUT)

            logger.info("Status: {}".format(res.status_code))
            logger.info("Headers: {}".format(res.headers))
            logger.info("Response: {}".format(res.content))

            result.response_status = res.status_code
            result.response_headers = res.headers

            if res.content is not None:
                result.response_payload = res.content

            if res.status_code == 200:
                if res.content is None:
                    result.message = result.description = "Empty payload"
                    raise ELPException()
                if res.headers['X-Backside-Transport'].startswith('FAIL'):
                    result.message = "Transaction failed"
                    if res.content:
                        pr = self.__parse_retailer2sub_loadresponse(res.content)
                        desc = respCodes[pr['response_code']]
                        result.parsed_data = pr
                        result.description = desc
                    raise ELPException()
                pr = self.__parse_retailer2sub_loadresponse(res.content)
                desc = respCodes[pr['response_code']]
                if pr['response_code'] != '0000':
                    result.description = desc
                    raise ELPException()
                result.ok = True
                result.parsed_data = pr
                result.description = desc
                txlogger.info("ELP {} Transaction complete.".format(operation),
                              extra={'user': 'SYSTEM',
                                     'operation': operation,
                                     'request_headers': result.request_headers,
                                     'request_payload': result.request_payload,
                                     'status_code': result.response_status,
                                     'response_headers': result.response_headers,
                                     'response_payload': result.response_payload,
                                     'description': result.description,
                                     'mobile_number': result.mobile_number,
                                     'status': 'SUCCESS'})
                logger.info("ELP {} Transaction complete.".format(operation))
                return result
            elif res.status_code == 401:
                if res.content is None:
                    result.message = result.description = "Empty payload"
                    raise ELPException()
                logger.info('Got error response')
                resp_dict = self._parse_error_response(res.content)
                desc = str(resp_dict['faultcode']) + str(' - ' + resp_dict['faultstring'])
                result.ok = False
                result.parsed_data = None
                result.description = desc
                raise ELPException()
            elif res.status_code == 500:
                logger.warning("Failed logon response, STATUS: {} HEADER: {} RESPONSE: {}".format(res.status_code,
                                                                                                  res.headers,
                                                                                                  res.content))
                result.ok = False
                result.parsed_data = None
                result.description = 'API Transaction Failure'
                raise ELPException()
            else:
                logger.warning("Unknown response, STATUS: {} HEADER: {} RESPONSE: {}".format(res.status_code,
                                                                                             res.headers,
                                                                                             res.content))
                result.ok = False
                result.parsed_data = None
                result.description = 'Unknown response error'
                raise ELPException()
        except ELPException as err:
            txlogger.error("ELP {} Transaction failed. {}: {}".format(operation,
                                                                      err,
                                                                      result.description), 
                           extra={'user': 'SYSTEM',
                                  'operation': operation,
                                  'request_headers': result.request_headers,
                                  'request_payload': result.request_payload,
                                  'status_code': result.response_status,
                                  'response_headers': result.response_headers,
                                  'response_payload': result.response_payload,
                                  'description': result.description,
                                  'mobile_number': result.mobile_number,
                                  'status': 'FAILED'})
            logger.error("ELP {} Transaction failed. {}: {}".format(operation,
                                                                    err,
                                                                    result.description))
            return result
        except Exception as err:
            txlogger.exception("ELP {} Transaction failed. {}: {}".format(operation,
                                                                          err,
                                                                          result.description),
                               extra={'user': 'SYSTEM',
                                      'operation': operation,
                                      'request_headers': result.request_headers,
                                      'request_payload': result.request_payload,
                                      'status_code': result.response_status,
                                      'response_headers': result.response_headers,
                                      'response_payload': result.response_payload,
                                      'description': result.description,
                                      'mobile_number': result.mobile_number,
                                      'status': 'FAILED'})
            logger.exception("ELP {} Transaction failed. {}: {}".format(operation,
                                                                        err,
                                                                        result.description))
            return result

    def __prepare_reload_form(self, flexiload=False, **kwargs):
        """
        :param kwargs:
        :return:

        Type of service being requested by the partner.
        - RTL2SUB = Reload Product Offer to Subscriber and Flexiload transactions
        - RTLSLD = Retailer Debit transactions.
        - RTLSLDREV = Retailer Debit reversal.
        - RTLMERL = Ilaw R2SEXT = PLDT RTLCGNL = Cignal

        """
        # xml_data = """<?xml version="1.0" encoding="UTF-8"?>
        # <soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:smar="http://smart.com.ph/">
        #     <soapenv:Header/>
        #     <soapenv:Body>
        #         <smar:retailerToSusbcriberRequest>
        #             <corporateID>{corp_id}</corporateID>
        #             <branchID>{branch_id}</branchID>
        #             <transactionKey>{transaction_key}</transactionKey>
        #             <requestRefNo>{ref_num}</requestRefNo>
        #             <planCode>{plan_code}</planCode>
        #             <!--Optional:-->
        #             <sourceAccount></sourceAccount>
        #             <transactionType>{transaction_type}</transactionType>
        #             <targetSubsAccount>{target_account}</targetSubsAccount>
        #             <!--Optional:-->
        #             <merchSenderNameFlag>false</merchSenderNameFlag>
        #             <!--Optional:-->
        #             <merchantCrossSell></merchantCrossSell>
        #             <requestTimestamp>1353916446</requestTimestamp>
        #             <terminalID>terminal01</terminalID>
        #             <address>Makati City</address>
        #             <!--Optional:-->
        #             <zipCode>1266</zipCode>
        #             <!--Optional:-->
        #             <unitCredit></unitCredit>
        #             <!--Optional:-->
        #             <targetCheckFlag></targetCheckFlag>
        #             <!--Optional:-->
        #             <amountDeduct></amountDeduct>
        #             <!--Optional:-->
        #             <transactionRRN></transactionRRN>
        #         </smar:retailerToSusbcriberRequest>
        #     </soapenv:Body>
        # </soapenv:Envelope>
        # """.format(**kwargs)

        xml_data = """<?xml version="1.0" encoding="UTF-8"?>
        <soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:smar="http://smart.com.ph/">
            <soapenv:Header/>
            <soapenv:Body>
                <smar:retailerToSusbcriberRequest>
                    <corporateID>{corp_id}</corporateID>
                    <branchID>{branch_id}</branchID>
                    <transactionKey>{transaction_key}</transactionKey>
                    <requestRefNo>{reference_number}</requestRefNo>
                    <planCode>{plan_code}</planCode>
                    <transactionType>{transaction_type}</transactionType>
                    <targetSubsAccount>{target_account}</targetSubsAccount>
                    <requestTimestamp>{request_timestamp}</requestTimestamp>
                    <terminalID>{terminal_id}</terminalID>
                    <address>{address}</address>
        """.format(**kwargs)

        if flexiload:
            xml_data += """            <unitCredit>{unit_credit}</unitCredit>""".format(**kwargs)

        xml_data += """
                </smar:retailerToSusbcriberRequest>
            </soapenv:Body>
        </soapenv:Envelope>
        """
        logger.info(xml_data)
        return xml_data

    def __parse_retailer2sub_loadresponse(self, data):
        response_dict = {}
        xml_response = et.fromstring(data)

        response_dict['corporateid_'] = xml_response.find('.//corporateID').text
        response_dict['branch_id'] = xml_response.find('.//branchID').text
        response_dict['reference_no'] = xml_response.find('.//requestRefNo').text
        response_dict['subscriber_acc'] = xml_response.find('.//targetSubsAccount').text
        response_dict['plan_code'] = xml_response.find('.//planCode').text
        response_dict['amount'] = xml_response.find('.//amount').text
        response_dict['retailer_deduction'] = xml_response.find('.//retailerDeduct').text
        response_dict['retailer_new_balance'] = xml_response.find('.//retailerNewBalance').text
        response_dict['transaction_refno'] = xml_response.find('.//transactionRRN').text     
        response_dict['response_code'] = xml_response.find('.//respCode').text
        response_dict['response_desc'] = xml_response.find('.//respCodeDesc').text
        response_dict['transaction_no'] = xml_response.find('.//transactionRRN').text
        response_dict['txtimestamp'] = xml_response.find('.//transactionTimestamp').text

        return response_dict

