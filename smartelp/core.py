# import os
# import binascii
import requests
import time
import logging
from decimal import Decimal


#from urlparse import urlparse

from six.moves.urllib.parse import urlparse

from base64 import b64encode
from lxml import etree as et

# from . import elp_settings
from .loggers import LogFileHandler, ELPTXLogger
from .helpers import (QueryResult, DictToObj, create_token, send_request, call_remote, generate_reference_code)
from .constants import respCodes
from .exceptions import ELPException

logging.basicConfig()
logger = logging.getLogger(__name__)
logger.setLevel(logging.DEBUG)

txlogger = ELPTXLogger.setup_logger()


class SmartAPI(object):
    def __init__(self, settings=None):
        
        if settings is None:
            raise ValueError('SmartELP settings not found')

        try:
            if isinstance(settings, dict):
                print('converting dict to object')
                self._config = DictToObj(settings)
            else:
                print('passing object as is')
                self._config = settings
            getattr(self._config, 'SMARTELP_HOST_URL')   
        except(AttributeError, Exception) as err:
            raise Exception('Invalid settings: %s' % err)          

        # self._config = elp_settings
        logger.info(self._config)
        o = urlparse(self._config.SMARTELP_HOST_URL)
        self._host_url = o.netloc.split(':')[0]
        self.merchSenderNameFlag = "ImFree"  # or True? Confusing documentation.

    def __get_auth_string(self, **kwargs):
        """
        :return: base64 encoded username and password
        """
        logger.info(kwargs)
        combined_auth_str = "{}:{}".format(kwargs.get('username'), kwargs.get('password'))
        auth_str = "Basic {}".format(b64encode(combined_auth_str.encode('utf-8')).decode())
        return auth_str

    def _get_timestamp(self):
        """
        :return: timestamp
        """
        return int(round(time.time()))

    def _get_reference_number(self):
        """
        :return: randomized 16 character string
        """
        return generate_reference_code()

    def _header_setup(self, **kwargs):
        """
        :param **kwards:
        :return: header content
        """
        headers = {"authorization": "{}".format(self.__get_auth_string(**kwargs)),
                   "accept-encoding": "gzip, deflate",
                   "content-type": "text/xml;charset=UTF-8",
                   "SOAPAction": kwargs.get('soap_action', ''),
                   "user-agent": "python-requests/2.10.0 CPython/2.7.10 Linux/3.2.0-30-generic",
                   "host": self._host_url,
                   }
                   # Content-Length
        return headers

    def _create_request(self, endpoint, payload, headers):
        logger.info("Preparing request parameters...")
        req = requests.Request('POST', url=endpoint, data=payload, headers=headers).prepare()
        return req

    def _prepare_balance_inquiry_form(self, **kwargs):
        """
        :param kwargs:
        :return: xml_data:

        Notes:
        Transaction Types:
        - DLRBAL - Dealer Balance Inquiry
        - RTLBAL - Retailer Balance Inquiry
        """
        xml_data = """<?xml version="1.0" encoding="UTF-8"?>
        <soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:smar="http://smart.com.ph/">
            <soapenv:Header/>
            <soapenv:Body>
                <smar:BalanceInquiryRequest>
                    <corporateID>{corp_id}</corporateID>
                    <branchID>{branch_id}</branchID>
                    <transactionKey>{transaction_key}</transactionKey>
                    <requestRefNo>{reference_number}</requestRefNo>
                    <transactionType>{transaction_type}</transactionType>
                    <requestTimestamp>{request_timestamp}</requestTimestamp>
                    <terminalID>{terminal_id}</terminalID>
                    <address>{address}</address>
                    <!--Optional:-->
                    <zipCode>{zip_code}</zipCode>
                    </smar:BalanceInquiryRequest>
            </soapenv:Body>
        </soapenv:Envelope>
        """.format(**kwargs)
        logger.info(xml_data)
        return xml_data

    def _parse_balance_inquiry_response(self, data):
        response_dict = {}
        xml_response = et.fromstring(data)

        response_dict['corporate_id'] = xml_response.find('.//corporateID').text
        response_dict['branch_id'] = xml_response.find('.//branchID').text
        response_dict['reference_no'] = xml_response.find('.//requestRefNo').text
        response_dict['wallet_balance'] = xml_response.find('.//walletBalance').text
        response_dict['response_code'] = xml_response.find('.//respCode').text
        response_dict['response_desc'] = xml_response.find('.//respCodeDesc').text
        response_dict['transaction_no'] = xml_response.find('.//transactionRRN').text
        response_dict['txtimestamp'] = xml_response.find('.//transactionTimestamp').text

        return response_dict

    def _parse_error_response(self, data):
        logger.info("Parsing response: {}".format(str(data)))
        response_dict = {}
        xml_response = et.fromstring(data)

        response_dict['faultcode'] = xml_response.find('.//faultcode').text
        response_dict['faultstring'] = xml_response.find('.//faultstring').text
        response_dict['transaction_id'] = xml_response.find('.//transaction-id').text
        response_dict['error_desc'] = xml_response.find('.//error-description').text

        return response_dict

    def get_balance(self, inquiry_type, reference_number=None, **kwargs):
        
        '''
        Get remaining balance of Dealer or Retailer entity

        :params inquiry_type: RTBAL or DLBAL
        :params kwargs: extra arguments of unknown purpose

        :return status:
        :return response: response tuple or None
        :return desc: response code description
        '''
        operation = inquiry_type
        txtimestamp = self._get_timestamp()

        if not reference_number:
            reference_number = self._get_reference_number()

        payload = dict()
        payload['corp_id'] = self._config.SMARTELP_CORPORATE_ID
        payload['branch_id'] = self._config.SMARTELP_BRANCH_ID
        payload['transaction_key'] = create_token(secretkey=self._config.SMARTELP_SECRET_KEY.strip(),
                                                  corporateid=self._config.SMARTELP_CORPORATE_ID.strip(),
                                                  branchid=self._config.SMARTELP_BRANCH_ID.strip(),
                                                  txtimestamp=txtimestamp)
        payload['reference_number'] = reference_number
        payload['transaction_type'] = inquiry_type
        payload['request_timestamp'] = txtimestamp
        payload['terminal_id'] = kwargs.get('terminal_id', self._config.SMARTELP_DEFAULT_POS_TERMINAL_ID)
        payload['address'] = kwargs.get('address', self._config.SMARTELP_DEFAULT_POS_ADDRESS)
        payload['zip_code'] = ""  # zipcode is optional

        xml_payload = self._prepare_balance_inquiry_form(**payload)
        credentials = {'username': self._config.SMARTELP_BALANCE_INQUIRY_USERNAME,
                       'password': self._config.SMARTELP_BALANCE_INQUIRY_PASSWORD}

        headers = self._header_setup(**credentials)
        req = self._create_request(self._config.SMARTELP_BALANCE_INQUIRY_URL,
                                   xml_payload,
                                   headers)

        logger.info("Headers: {}".format(headers))
        logger.info("Endpoint@: {}".format(self._config.SMARTELP_BALANCE_INQUIRY_URL))

        s = requests.Session()
        result = QueryResult()
        
        result.request_headers = headers
        result.request_payload = xml_payload

        try:
            res = call_remote(fn=send_request,
                              payload=payload,
                              session=s,
                              request_obj=req,
                              timeout=self._config.SMARTELP_REQUEST_TIMEOUT)

            logger.info("Status: {}".format(res.status_code))
            logger.info("Headers: {}".format(res.headers))
            logger.info("Response: {}".format(res.content))

            result.response_status = res.status_code
            result.response_headers = res.headers

            if res.content is not None:
                result.response_payload = res.content

            if res.status_code == 200:
                if res.content is None:
                    result.message = result.description = "Empty payload"
                    raise ELPException()
                if res.headers['X-Backside-Transport'].startswith('FAIL'):
                    result.message = "Transaction failed"
                    if res.content:
                        pr = self._parse_balance_inquiry_response(res.content)
                        desc = respCodes[pr['response_code']]
                        result.parsed_data = pr
                        result.description = desc
                    raise ELPException()
                pr = self._parse_balance_inquiry_response(res.content)
                desc = respCodes[pr['response_code']]
                if pr['response_code'] != '0000':
                    result.description = desc
                    raise ELPException()
                result.ok = True
                result.parsed_data = pr
                result.description = desc
                txlogger.info("ELP {} Transaction complete.".format(operation),
                              extra={'user': 'SYSTEM',
                                     'operation': operation,
                                     'request_headers': result.request_headers,
                                     'request_payload': result.request_payload,
                                     'status_code': result.response_status,
                                     'response_headers': result.response_headers,
                                     'response_payload': result.response_payload,
                                     'description': result.description,
                                     'status': 'SUCCESS'})

                logger.info("ELP {} Transaction complete.".format(operation))
                return result
            elif res.status_code == 401:
                if res.content is None:
                    result.message = result.description = "Empty payload"
                    raise ELPException()
                logger.info('Got error response')
                resp_dict = self._parse_error_response(res.content)
                desc = str(resp_dict['faultcode']) + str(' - ' + resp_dict['faultstring'])
                result.ok = False
                result.parsed_data = None
                result.description = desc
                raise ELPException()
            elif res.status_code == 500:
                logger.warning("Failed logon response, STATUS: {} HEADER: {} RESPONSE: {}".format(res.status_code, res.headers, res.content))
                result.ok = False
                result.parsed_data = None
                result.description = 'API Transaction Failure'
                raise ELPException()
            else:
                logger.warning("Unknown response, STATUS: {} HEADER: {} RESPONSE: {}".format(res.status_code, res.headers, res.content))
                result.ok = False
                result.parsed_data = None
                result.description = 'Unknown response error'
                raise ELPException()
        except ELPException as err:
            txlogger.error("ELP {} Transaction failed. {}: {}".format(operation,
                                                                      err,
                                                                      result.description),
                           extra={'user': 'SYSTEM',
                                  'operation': operation,
                                  'request_headers': result.request_headers,
                                  'request_payload': result.request_payload,
                                  'status_code': result.response_status,
                                  'response_headers': result.response_headers,
                                  'response_payload': result.response_payload,
                                  'description': result.description,
                                  'status': 'FAILED'})
            logger.error("ELP {} Transaction failed. {}: {}".format(operation,
                                                                    err,
                                                                    result.description))
            return result
        except Exception as err:
            txlogger.exception("ELP {} Transaction failed. {}: {}".format(operation,
                                                                          err,
                                                                          result.description),
                               extra={'user': 'SYSTEM',
                                      'operation': operation,
                                      'request_headers': result.request_headers,
                                      'request_payload': result.request_payload,
                                      'status_code': result.response_status,
                                      'response_headers': result.response_headers,
                                      'response_payload': result.response_payload,
                                      'description': result.description,
                                      'status': 'FAILED'})
            logger.exception("ELP {} Transaction failed. {}: {}".format(operation,
                                                                        err,
                                                                        result.description))
            return result
