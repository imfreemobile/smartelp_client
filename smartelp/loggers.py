from __future__ import absolute_import

import logging
from logging.handlers import RotatingFileHandler

from . import elp_settings


class ELPTXLogger(object):
    @staticmethod
    def setup_logger():
        txlogger = logging.getLogger('elptx')
        if not len(txlogger.handlers):
            try:
                using_django = getattr(elp_settings, 'USING_DJANGO')
                if not using_django:
                    logfile = elp_settings.SMARTELP_TXLOG_FILE
                    filehandler = LogFileHandler(filename=logfile)
                    formatter = ELPLogFormatter("%(asctime)s %(operation)s %(levelname)s %(status)s - %(message)s")
                    filehandler.setFormatter(formatter)
                    txlogger.addHandler(filehandler)
                    txlogger.setLevel(logging.INFO)
                    return txlogger
            except AttributeError as ae:
                pass
        return txlogger


class LogFileHandler(RotatingFileHandler):
    
    def __init__(self, filename) :
        RotatingFileHandler.__init__(self, filename=filename)

    # def emit(self, record):
    #     # Todo: Implemetation Here
    #     pass


class ELPLogFormatter(logging.Formatter):
    log_fmt = "%(asctime)s [%(operation)s]: %(levelname)s %(status)s - %(message)s"
    
    def __init__(self, fmt="%(levelno)s: %(msg)s)"):
        logging.Formatter.__init__(self, fmt)

    def format(self, record):
        self._fmt = ELPLogFormatter.log_fmt
        result = logging.Formatter.format(self, record)
        return result
