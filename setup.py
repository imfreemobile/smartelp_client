import os
from setuptools import find_packages, setup

with open(os.path.join(os.path.dirname(__file__), 'README.md')) as readme:
    README = readme.read()

# allow setup.py to be run from any path
os.chdir(os.path.normpath(os.path.join(os.path.abspath(__file__), os.pardir)))

setup(
    name='smartelp-client',
    version='0.0.22',
    packages=find_packages(),
    include_package_data=True,
    install_requires=[
        'phonenumberslite>=7.7.4',
        'requests>=2.10.0',
        'lxml>=3.6.4'
    ],
    license='BSD License',  # example license
    description='Smart ELP client in Python.',
    long_description=README,
    url='N/A',
    author='Louie Tanalas',
    author_email='ltanalas@gmail.com',
    classifiers=[
        'Development Status :: 2 - Pre-Alpha'
        'Environment :: Web Environment',
        'Environment :: Console',
        'Intended Audience :: Developers',
        'License :: OSI Approved :: BSD License',
        'Operating System :: OS Independent',
        'Programming Language :: Python',
        'Programming Language :: Python :: 2.7',
        'Programming Language :: Python :: 3.5',
        'Topic :: Internet :: WWW/HTTP',
        'Topic :: Internet :: WWW/HTTP :: Dynamic Content',
    ],
)
