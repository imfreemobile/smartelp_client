# Development Setup

## Installation
```
pip install -e git+ssh://git@bitbucket.org/imfree/smartelp_client.git@master#egg=smartelp-client
```

## Non-Django Apps
  * must have a smartelp.ini config file either in home directory, current application directory, or in any path as long as it is defined in SMARTELP_CONF environment variable.
  * must have the following fields:
  eg:

```
[DEFAULT]
SMARTELP_HOST_URL = https://stg.apis.smart.com.ph:7443
SMARTELP_DEALER2RETAILER_URL = https://stg.apis.smart.com.ph:7443/DealerToRetailer
SMARTELP_DEALER2RETAILER_USERNAME = imfree_user
SMARTELP_DEALER2RETAILER_PASSWORD = user@firstmobile
SMARTELP_BALANCE_INQUIRY_URL = https://stg.apis.smart.com.ph:7443/BalanceInquiry
SMARTELP_BALANCE_INQUIRY_USERNAME = imfree_baluser
SMARTELP_BALANCE_INQUIRY_PASSWORD = baluser@imfree
SMARTELP_RETAILER2SUBSCRIBER_URL = https://stg.apis.smart.com.ph:7443/RetailerToSubscriberLoading
SMARTELP_RETAILER2SUBSCRIBER_USERNAME = imfree_baluser
SMARTELP_RETAILER2SUBSCRIBER_PASSWORD = baluser@imfree
SMARTELP_REQUEST_TIMEOUT = 70
SMARTELP_SECRET_KEY = eiMGqGpqaIXFCskp
SMARTELP_CORPORATE_ID = ImfreeSkywalk3r
SMARTELP_BRANCH_ID = IMF000000000015955
SMARTELP_DEALER_ACC_NO = 55775193210251
SMARTELP_DEFAULT_RETAILER_ACC_NO = 09491975494
SMARTELP_DEFAULT_POS_TERMINAL_ID = n/a
SMARTELP_DEFAULT_POS_ADDRESS = n/a
```

## Python configs

elp_settings={
    "SMARTELP_HOST_URL":"https://stg.apis.smart.com.ph:7443",
    "SMARTELP_DEALER2RETAILER_URL":"https://stg.apis.smart.com.ph:7443/DealerToRetailer",
    "SMARTELP_DEALER2RETAILER_USERNAME":"imfree_user",
    "SMARTELP_DEALER2RETAILER_PASSWORD":"user@firstmobile",
    "SMARTELP_BALANCE_INQUIRY_URL":"https://stg.apis.smart.com.ph:7443/BalanceInquiry",
    "SMARTELP_BALANCE_INQUIRY_USERNAME":"imfree_user",
    "SMARTELP_BALANCE_INQUIRY_PASSWORD":"user@firstmobile",
    "SMARTELP_RETAILER2SUBSCRIBER_URL":"https://stg.apis.smart.com.ph:7443/RetailerToSubscriberLoading",
    "SMARTELP_RETAILER2SUBSCRIBER_USERNAME":"imfree_user",
    "SMARTELP_RETAILER2SUBSCRIBER_PASSWORD":"user@firstmobile",
    "SMARTELP_REQUEST_TIMEOUT":70,
    "SMARTELP_SECRET_KEY":"eiMGqGpqaIXFCskp",
    "SMARTELP_CORPORATE_ID":"ImfreeSkywalk3r",
    "SMARTELP_BRANCH_ID":"IMF000000000015955",
    "SMARTELP_DEALER_ACC_NO":"55775193210251",
    "SMARTELP_DEFAULT_RETAILER_ACC_NO":"09491975494",
    "SMARTELP_SUN_RETAILER_ACC_NO":"09339321576",
    "SMARTELP_DEFAULT_POS_TERMINAL_ID":"n/a",
    "SMARTELP_DEFAULT_POS_ADDRESS":"n/a",
}



## Django Apps
  * must include the following fields in Django settings
  * eg:

```ini
SMARTELP_HOST_URL = 'https://stg.apis.smart.com.ph:7443'
SMARTELP_DEALER2RETAILER_URL = 'https://stg.apis.smart.com.ph:7443/DealerToRetailer'
SMARTELP_DEALER2RETAILER_USERNAME = 'imfree_user'
SMARTELP_DEALER2RETAILER_PASSWORD = 'user@firstmobile'
SMARTELP_BALANCE_INQUIRY_URL = 'https://stg.apis.smart.com.ph:7443/BalanceInquiry'
SMARTELP_BALANCE_INQUIRY_USERNAME = 'imfree_baluser'
SMARTELP_BALANCE_INQUIRY_PASSWORD = 'baluser@imfree'
SMARTELP_RETAILER2SUBSCRIBER_URL = 'https://stg.apis.smart.com.ph:7443/RetailerToSubscriberLoading'
SMARTELP_RETAILER2SUBSCRIBER_USERNAME = 'imfree_baluser'
SMARTELP_RETAILER2SUBSCRIBER_PASSWORD = 'baluser@imfree'
SMARTELP_REQUEST_TIMEOUT = 70
SMARTELP_SECRET_KEY = 'eiMGqGpqaIXFCskp'
SMARTELP_CORPORATE_ID = 'ImfreeSkywalk3r'
SMARTELP_BRANCH_ID = 'IMF000000000015955'
SMARTELP_DEALER_ACC_NO = '55775193210251'
SMARTELP_DEFAULT_RETAILER_ACC_NO = '09491975494'
SMARTELP_DEFAULT_POS_TERMINAL_ID = 'n/a'
SMARTELP_DEFAULT_POS_ADDRESS = 'n/a'
```

## Usage:
### Balance Inquiry
```python

# Django shell
# use python manage.py shell (or shell_plus)

from smartelp import Dealer, Retailer, elp_settings

if elp_settings is None:
  # Define Configs here
dealer = Dealer(elp_settings)

# Dealer Balance Inquiry
dbal = dealer.get_balance()
print dbal.__dict__

# Dealer to Retailer Fund Transfer (SMART/TNT Account)
d2smart = dealer.topup_retailer_account(amount=200.00):
print d2smart.__dict__

# Dealer to Retailer Fund Transfer (Sun Account)
d2sun = dealer.topup_retailer_account(amount=200.00, kwargs={'retailer_account': elp_settings.SMARTELP_SUN_RETAILER_ACC_NO})
print d2sun.__dict__


retailer = Retailer(elp_settings)

# Smart/TNT retailer account balance Inquiry
smart_rbal = retailer.get_balance()
print smart_rbal.__dict__

# Smart/TNT retailer account balance Inquiry
sun_rbal = retailer.get_balance(inquiry_type="RTLBALSUN")
print sun_rbal.__dict__

# Dealer to Subscriber (Smart/TNT)
# MYLOAD
r2sub_smart_myload = retailer.topup_subscriber(plan='MYLOAD', mobile_number='+639491147803', amount=5.00)
print r2sub_smart_myload.__dict__

# ELP Keyword, no need to add amount param
r2sub_smart = retailer.topup_subscriber(plan='W15', mobile_number='+639491147803')
print r2sub_smart.__dict__


# Dealer to Subscriber (SUN)
r2sub_sun = retailer.topup_subscriber(plan='WRTSWIN10', mobile_number='+63SUNNUMBER', transaction_type="RTL2SUBSUN")
print r2sub_sun.__dict__
```

